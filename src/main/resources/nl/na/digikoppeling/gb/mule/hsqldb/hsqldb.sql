--
-- Copyright 2017 Nationaal Archief
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

create table gb_transaction
(
	id								VARCHAR(256)	PRIMARY KEY,
	timestamp					TIMESTAMP			NOT NULL,
	filename					VARCHAR(256)	NOT NULL,
	content_type			VARCHAR(256)	NOT NULL,
	url								VARCHAR(256)	NOT NULL,
	max_file_size			BIGINT				NOT NULL,
	compression				TINYINT				DEFAULT 0 NOT NULL,
	data_reference		CLOB					NULL,
	status						TINYINT				DEFAULT 0 NOT NULL,
	status_time				TIMESTAMP			DEFAULT NOW() NOT NULL,
	status_message		CLOB					NULL
);

create table gb_file
(
	gb_transaction_id	VARCHAR(256)	NOT NULL,
	filename					VARCHAR(256)	NOT NULL,
	status						TINYINT				DEFAULT 0 NOT NULL,
	FOREIGN KEY (gb_transaction_id) REFERENCES gb_transaction(id)
);